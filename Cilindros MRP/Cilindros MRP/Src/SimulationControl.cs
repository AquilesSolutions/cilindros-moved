﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cilindros_MRP.Src
{

    public class SimulationControl
    {
        private int SimulationsToRun = 100;
        private List<Simulation> simulations;
        private List<DataSets.Result> results;
        private UserControl users;
        private PopulateDataSet getData;
        private int index = 94;

        public SimulationControl(UserControl users)
        {
            getData = new PopulateDataSet();
            getData.populate();
            simulations = new List<Simulation>();
            results = new List<DataSets.Result>();
            this.users = users;

            for (int i = 0; i < SimulationsToRun; i++)
            {
                simulations.Add(new Simulation(getData.Data));
            }

            
        }

        public SimulationControl(List<DataSets.Result> results, DataSets.DataSource source, UserControl user)
        {
            getData = new PopulateDataSet();
            getData.Data = source;

            this.results = results;
            users = user;
        }

        public DataSets.Result start()
        {
            for (int i = 0; i < SimulationsToRun; i++)
            {
                results.Add(simulations[i].Start());

            }

            results.Sort(DataSets.Result.CompareByPurchases);
            save();

            return results[index];
        }

        public void save()
        {
            users.autoSaveSimulation(getData.Data, results, simulations);
        }

        public DataSets.Result getResult
        {
            get { return results[index]; }
        }
    }
}
