﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cilindros_MRP.Src
{
    class PopulateDataSet
    {
        private DataSets.DataSource results;
        private String format = "MMM yyyy";

        public PopulateDataSet()
        {
            results = new DataSets.DataSource();
        }

        public DataSets.DataSource Data
        {
            get { return results; }
            set { results = value; }
        }

        public void populate()
        {
            String file = @"Data/Production.txt";
            String table = "Prediction";

            readFile(file, table);

            file = @"Data/Schema.txt";
            table = "Schemas";

            readFile(file, table);

            file = @"Data/Ratios.txt";
            table = "Ratios";

            readFile(file, table);

            file = @"Data/Inventory.txt";
            table = "Inventory";

            readFile(file, table);

            file = @"Data/Groove.txt";
            table = "Groove";

            readFile(file, table);
        }

        public void readFile(String file, String table)
        {
            int counter = 0;
            string line;

            System.IO.StreamReader reader =
                new System.IO.StreamReader(file);
            while ((line = reader.ReadLine()) != null)
            {
                addToTable(table, line);
                counter++;
            }

            reader.Close();
        }

        public void addToTable(String tableName, String data)
        {

            String[] splitData = data.Split(',');

            if(tableName == "Prediction")
            {
                DataTable month = results.Tables["Period"];
                
                for(int i = 2; i < splitData.Length; i+=2)
                {
                    DataRow rowm = month.NewRow();
                    string[] date = splitData[i].Split('/');
                    rowm[0] = new DateTime(int.Parse(date[2]), int.Parse(date[1]), int.Parse(date[0]));
                    month.Rows.Add(rowm);
                }

                DataTable quantity = results.Tables["Quantities"];
                DataRow row = quantity.NewRow();
                int count = 1;
                for (int i = 1; i < splitData.Length; i += 2)
                {
                    row[count] = splitData[i];
                    count++;
                }

                row[0] = splitData[0];
                quantity.Rows.Add(row);


            } else
            {
                DataTable table = results.Tables[tableName];
                DataRow row = table.NewRow();

                switch (tableName)
                {
                    case "Inventory":
                        for(int i = 0; i < splitData.Length; i++)
                        {
                            row[i] = splitData[i];
                        }
                        break;

                    case "Groove":
                        row[0] = splitData[0];
                        row[1] = splitData[1];
                        row[2] = splitData[3];
                        break;

                    case "Ratios":
                        for(int i =1; i < splitData.Length; i+=2)
                        {
                            table.Columns.Add("Groove" + i);
                            table.Columns.Add("Ratio" + i);
                        }

                        row = table.NewRow();
                        for(int i = 0; i < splitData.Length; i++)
                        {
                            row[i] = splitData[i];
                        }
                        break;
                    case "Schemas":
                        for(int i = 1; i < 11; i++)
                        {
                            row[i - 1] = splitData[i];
                        }
                        row[10] = splitData[11];
                        row[11] = splitData[0];
                        break;
                }

                table.Rows.Add(row);
            }
            /*String[] splitdata = data.Split(',');
            DataTable table = results.Tables[tableName];
            DataRow row = table.NewRow();

            for (int i = 0; i < splitdata.Length; i++)
            {
                row[i] = splitdata[i];
            }

            if(tableName == "Prediction")
            {
                row[3] = splitdata[0] + splitdata[1];
                DataTable table2 = results.Tables["Total Production"];
                

                Double quantity = Double.Parse(splitdata[2]);
                String product = splitdata[1];

                String[] dateComponents = splitdata[0].Split('/');
                DateTime date2 = new DateTime(Int16.Parse(dateComponents[2]), Int16.Parse(dateComponents[1]), Int16.Parse(dateComponents[0]));

                DataRow row2 = table2.NewRow();
                    //Month, product, prediction
                    row2[0] = date2.ToString(format);
                    row2[1] = product;
                    row2[2] = quantity;

                    row2[3] = quantity * 1.8;
                    row2[4] = quantity * 0.2;
                    table2.Rows.Add(row2);

                DataTable products = results.Tables["Products"];
                DataRow productRow = products.Rows.Find(product);

                if(productRow != null)
                {
                    table2 = results.Tables[productRow[1].ToString()];
                    
                    row2 = table2.Rows.Find(date2.ToString(format));

                    ///row2[0] = splitdata[1];
                    row2[1] = quantity;
                    row2[2] = quantity * 1.8;
                    row2[3] = quantity * 0.2;
                } else
                {
                    products.Rows.Add(new object[] { product, product + " production" });
                    DataTable newTable = new DataTable(product + " production");
                    DataColumn month = new DataColumn("Month");

                    DataColumn predicted = new DataColumn("Predicted", Type.GetType("System.Int64"));
                    DataColumn max = new DataColumn("Max", Type.GetType("System.Int64"));
                    DataColumn min = new DataColumn("Min", Type.GetType("System.Int64"));
                    newTable.Columns.Add(month);
                    newTable.PrimaryKey = new DataColumn[] { month };
                    newTable.Columns.Add(predicted);
                    newTable.Columns.Add(max);
                    newTable.Columns.Add(min);

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime date = new DateTime(2015, i + 1, 1);
                        newTable.Rows.Add(new object[] { date.ToString(format), 0, 0, 0 });
                    }

                    results.Tables.Add(newTable);
                }

                if(!results.Tables.Contains(product + " production"))
                {
                    //Should never happen now
                }

                
                //table2.Rows.Add(row2);
            }

            table.Rows.Add(row);*/
        }
    }
}
