﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Security.Permissions;

namespace Cilindros_MRP.Src
{
    public class UserControl
    {

        private Dictionary<String, String> passwords;
        private Dictionary<String, String> details;
        private Forms.MainWindow APPWINDOW;
        private FileHandler storage;
        private String logonidentifier;
        private String UN;

        public Dictionary<String, String> Details {
            get { return details; }
        }

        public UserControl(Forms.MainWindow a)
        {
            logonidentifier = Environment.MachineName + "," + DateTime.UtcNow.ToString() + " UTC";
            APPWINDOW = a;
            passwords = new Dictionary<String, String>();
            details = new Dictionary<String, String>();
            storage = new FileHandler(1000);
            loadUsers();
            monitorUsers();
        }

        public string logon(string user, string password)
        {
            if (passwords.ContainsKey(user))
            {
                if(passwords[user] == GetMd5Hash(password))
                {
                    UN = user;
                    return "true";
                } else
                {
                    return "Incorrect password";
                }
            }
            return "Incorrect user name";
        }

        public string multiple(string user)
        {
            if (storage.existsFile(@"Data/" + user + @"/" + user + ".lgn"))
            {
                return "User already logged on at: " + storage.readFromFile(@"Data/" + user + @"/" + user + ".lgn");
            } else
            {
                storage.createDirectory(@"Data/" + user);
                string userString = logonidentifier;
                byte[] userbytes = Encoding.UTF8.GetBytes(userString);
                if (storage.concurrentAllowAccessCheck(@"Data/" + user + @"/", user, 1000))
                {
                    storage.writeToFile(@"Data/" + user + @"/" + user + ".lgn", userbytes);
                    storage.releaseAccess(@"Data/" + user + @"/", user);
                    return "true";
                }
                return "File Access Timeout";
            }
        }

        public void overRideUser(string userName)
        {
            string path = @"Data/" + userName + @"/" + userName + ".lgn";
            byte[] userbytes = Encoding.UTF8.GetBytes(logonidentifier);
            if (storage.concurrentAllowAccessCheck(@"Data/" + userName + @"/", userName, 1000))
            {
                storage.writeToFile(path, userbytes);
                storage.releaseAccess(@"Data/" + userName + @"/", userName);
            }
        }

        public void logOut(string user)
        {
            string path = @"Data/" + user + @"/" + user + ".lgn";
            if (storage.concurrentAllowAccessCheck(@"Data/" + user + @"/", user, 1000))
            {
                string result = storage.readFromFile(path);

                if (result.Equals(logonidentifier.ToString()))
                {
                    storage.delete(path);
                }
                storage.releaseAccess("Data/" + user + @"/", user);
            }
        }

        public string getDetails(string user)
        {
            return details[user];
        }

        public void updateDetails(string user, string update)
        {
            details[user] = update;
            updateAllUsers();

        }

        public bool updatePassword(string user, string password)
        {
            if (passwords.ContainsKey(user))
            {
                passwords[user] = GetMd5Hash(password);
                updateAllUsers();
                return true;
            }
            return false;
        }

        private bool loadUsers()
        {
            if (!storage.existsFile(@"Data/Users/users"))
            {
                storage.createDirectory(@"Data/Users");
                string userString = "admin," + GetMd5Hash("admin") + "," + "Administrator" + Environment.NewLine;
                byte[] userbytes = Encoding.UTF8.GetBytes(userString);
                if (storage.concurrentAllowAccessCheck(@"Data/Users/", "users", 1000)) {
                    storage.writeToFile(@"Data/Users/users", userbytes);
                    passwords["admin"] = GetMd5Hash("admin");
                    details["admin"] = "Administrator";
                    storage.releaseAccess(@"Data/Users/", "users");
                    return true;
                }
                return false;
            } else
            {
                if (storage.concurrentAllowAccessCheck(@"Data/Users/", "users", 1000))
                {
                    string contents = storage.readFromFile(@"Data/Users/users");

                    String[] results = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                    for (int i = 0; i < results.Length; i++)
                    {
                        if (results[i] != "")
                        {
                            string[] result = results[i].Split(',');
                            passwords[result[0]] = result[1];
                            if (result.Length > 3)
                            {
                                string rd = result[2];
                                for (int j = 3; j < result.Length; j++)
                                {
                                    rd += "," + result[j];
                                }
                                details[result[0]] = rd;
                            }
                            else
                            {
                                details[result[0]] = result[2];
                            }

                        }
                    }
                    storage.releaseAccess(@"Data/Users/", "users");
                    return true;
                } else
                {
                    return false;
                }
            }
        }

        public bool updateAllUsers()
        {
            String[] keys = new String[passwords.Keys.Count];
            passwords.Keys.CopyTo(keys, 0);
            String userString = "";

            for(int i =0; i < keys.Length; i++)
            {
                userString += keys[i] + "," + passwords[keys[i]] + "," + details[keys[i]] + Environment.NewLine;
            }
            byte[] userbytes = Encoding.UTF8.GetBytes(userString);
            if (storage.concurrentAllowAccessCheck(@"Data/Users/", "users", 1000))
            {
                storage.writeToFile(@"Data/Users/users", userbytes);
                storage.releaseAccess(@"Data/Users/", "users");
                return true;
            }
            return false;
        }

        private void reloadUsers()
        {
            passwords = new Dictionary<System.String, System.String>();
            details = new Dictionary<System.String, System.String>();
            loadUsers();
        }

        public bool addUser(string userName, string passwd, string userDetails)
        {
            if (passwords.ContainsKey(userName))
            {
                return false;
            }
            passwords[userName] = GetMd5Hash(passwd);
            details[userName] = userDetails;

            updateAllUsers();

            return true;
        }

        public bool removeUser(string userName)
        {
            if (passwords.ContainsKey(userName))
            {
                passwords.Remove(userName);
                details.Remove(userName);
                updateAllUsers();
                return true;
            } else
            {
                return false;
            }
            
        }

        /*public bool updateUserDetails(string userName, string passwd, string userDetails)
        {
            if (passwords.ContainsKey(userName))
            {
                passwords[userName] = GetMd5Hash(passwd);
                details[userName] = userDetails;
                updateAllUsers();
            }
            return false;
        }
        */
        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public void monitorUsers()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = @"Data/Users/";
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "users";

            watcher.Changed += new FileSystemEventHandler(usersUpdated);

            watcher.EnableRaisingEvents = true;
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public void monitorLogon(String user)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = @"Data/" + user + @"/";
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = user + ".lgn";

            watcher.Changed += new FileSystemEventHandler(userLogonUpdated);

            watcher.EnableRaisingEvents = true;
        }

        public void usersUpdated(Object source, FileSystemEventArgs e)
        {
            reloadUsers();
        }

        public void userLogonUpdated(Object source, FileSystemEventArgs e)
        {
            APPWINDOW.Invoke((System.Windows.Forms.MethodInvoker)delegate
           {
               APPWINDOW.Close();
           });
        }

        public void SaveData(System.Data.DataSet data)
        {
            String path = @"Data/" + UN + @"/results " + DateTime.Now.ToString("dd-mmm-yyyy:HH:mm") + ".dat";
            storage.WriteData(data, path);
        }

        public String[] getDataFiles()
        {
            return storage.getDataFiles(@"Data/" + UN + @"/");
        }

        public void LoadData(System.Data.DataSet data, String path)
        {
            String[] p = path.Split('/');
            if (storage.concurrentAllowAccessCheck(@"Data/" + UN + @"/", p[p.Length-1], 1000))
            {
                storage.ReadData(data, path);
                storage.releaseAccess(@"Data/" + UN + @"/", p[p.Length - 1]);
            }
        }

        public void autoSaveSimulation(DataSets.DataSource source, List<DataSets.Result> results, List<Simulation> sims)
        {
            String path = @"Data/" + UN + "/";
            storage.autoSaveSimulation(path, source, results, sims);
        }

        public string[] getSavedSims()
        {
            return storage.getSavedSims(@"Data/" + UN + "/");
        }

        public string getSimName(String path)
        {
            return storage.getSimulationName(path);
        }

        public SimulationControl loadSavedSim(string folder)
        {
            return storage.loadSavedSimulation(@"Data/" + UN + "/" + folder, this);
        }

    }
}
