﻿using System;
using System.Windows.Forms;

namespace Cilindros_MRP.Forms
{
    public partial class LogonWindow : Form
    {
        MainWindow app;

        public LogonWindow(MainWindow window)
        {
            app = window;
            InitializeComponent();

        }

        private void logon(object sender, EventArgs e)
        {
            string user = name.Text;
            string pswrd = password.Text;

            string result = app.checkUser(user, pswrd);

            if (result.Equals("true")) {
                    app.enable();
                    Close();
            } else {
                if (result.Contains("User already logged on at"))
                {
                    DialogResult dr = MessageBox.Show(result + "\r\nLoggong on again will cause the first session to be closed and any unsaved data will be lost.\r\nDo you want to continue?", "Already logged on", MessageBoxButtons.YesNo);
                    if(dr == DialogResult.Yes)
                    {
                        app.overRideUser(user);
                        app.enable();
                        Close();
                    } else
                    {
                        password.Text = "";
                        name.Text = "";
                    }
                }
                else
                {
                    incorrect.Visible = true;
                    incorrect.Text = result;
                    password.Text = "";
                    name.Text = "";
                }
            }
        }

        private void cancel(object sender, EventArgs e)
        {
            app.Close();
            Close();
        }

        private void userName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar== (char)13)
            {
                logonButton.PerformClick();
            }
        }
    }
}
