﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cilindros_MRP.Src.DataObjects
{
    class Product
    {
        private int amount;
        private List<String> requiredGrooves;
        private List<double> ratios;
        private string productGroup;

        public Product(String Group, int amountToProduce, List<string> grooves, List<Double> gRatios)
        {
            productGroup = Group;
            amount = amountToProduce;
            requiredGrooves = grooves;
            ratios = gRatios;   
        }

        public string ProductGroup
        {
            get { return productGroup; }
        }

        public int Quantity
        {
            get { return amount; }
        }

        public List<String> Gooves
        {
            get { return requiredGrooves; }
        }

        public List<double> Ratio
        {
            get { return ratios; }
        }

        public void getGroovesAndRatios()
        {

        }
    }
}
