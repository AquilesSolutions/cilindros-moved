﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cilindros_MRP.Src.DataObjects
{
    class Schema
    {
        private string id;
        private List<Groove> grooves;
        private int consumes;

        public Schema(string id, List<Groove> g, int amount)
        {
            this.id = id;
            grooves = g;
            consumes = amount;
        }

        public string ID
        {
            get { return id; }
        }

        public List<Groove> Grooves
        {
            get { return grooves; }
        }

        public int Consume
        {
            get { return consumes; }
        }
    }
}
