﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cilindros_MRP.Controls
{
    public partial class ControlAddUser : UserControl
    {
        Src.UserControl users;
        public ControlAddUser(Src.UserControl u)
        {
            users = u;
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            bool super = superUserCheck.Checked;
            bool admin = adminCheck.Checked;

            String details = "";

            if (admin)
            {
                details = "Administrator";
            }
            if (super)
            {
                if(details != "")
                {
                    details += ",SuperUser";
                } else
                {
                    details = "SuperUser";
                }
            } else
            {
                if (details != "")
                {
                    details += "," + installations.Text;
                }
                else
                {
                    details = installations.Text;
                }
            }
            if (userName.Text == "")
            {
                status.Text = "A User Name is required";
            } else if(password.Text == "")
            {
                status.Text = "A password is required";
            } else if(details ==  "")
            {
                status.Text = "Please add installations or set user to Admin or Super User";
            }

            if (users.addUser(userName.Text, password.Text, details)){
                status.Text = "User " + userName.Text + " added";
            } else
            {
                status.Text = "User Name " + userName.Text + "is already used";
            }
        }

        private void superUserCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (superUserCheck.Checked)
            {
                installations.Enabled = false;
            } else
            {
                installations.Enabled = true;
            }
        }
    }
}
