﻿namespace Cilindros_MRP.Forms
{
    partial class AdminPasswordChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.userLabel = new DevExpress.XtraEditors.LabelControl();
            this.password1Label = new DevExpress.XtraEditors.LabelControl();
            this.password2Label = new DevExpress.XtraEditors.LabelControl();
            this.username = new DevExpress.XtraEditors.LabelControl();
            this.password1 = new DevExpress.XtraEditors.TextEdit();
            this.password2 = new DevExpress.XtraEditors.TextEdit();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.password1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.password2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(28, 23);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(186, 19);
            this.title.TabIndex = 40;
            this.title.Text = "Change User Password";
            // 
            // userLabel
            // 
            this.userLabel.Location = new System.Drawing.Point(98, 74);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(59, 13);
            this.userLabel.TabIndex = 41;
            this.userLabel.Text = "User Name: ";
            // 
            // password1Label
            // 
            this.password1Label.Location = new System.Drawing.Point(80, 117);
            this.password1Label.Name = "password1Label";
            this.password1Label.Size = new System.Drawing.Size(77, 13);
            this.password1Label.TabIndex = 42;
            this.password1Label.Text = "New password: ";
            // 
            // password2Label
            // 
            this.password2Label.Location = new System.Drawing.Point(41, 162);
            this.password2Label.Name = "password2Label";
            this.password2Label.Size = new System.Drawing.Size(116, 13);
            this.password2Label.TabIndex = 43;
            this.password2Label.Text = "Confirm new password: ";
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(202, 74);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(0, 13);
            this.username.TabIndex = 44;
            // 
            // password1
            // 
            this.password1.Location = new System.Drawing.Point(202, 114);
            this.password1.Name = "password1";
            this.password1.Size = new System.Drawing.Size(150, 20);
            this.password1.TabIndex = 45;
            // 
            // password2
            // 
            this.password2.Location = new System.Drawing.Point(202, 159);
            this.password2.Name = "password2";
            this.password2.Size = new System.Drawing.Size(150, 20);
            this.password2.TabIndex = 46;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(296, 239);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(95, 23);
            this.okButton.TabIndex = 47;
            this.okButton.Text = "Change Password";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(202, 239);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 48;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // AdminPasswordChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 291);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.password2);
            this.Controls.Add(this.password1);
            this.Controls.Add(this.username);
            this.Controls.Add(this.password2Label);
            this.Controls.Add(this.password1Label);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AdminPasswordChange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Password";
            ((System.ComponentModel.ISupportInitialize)(this.password1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.password2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.LabelControl userLabel;
        private DevExpress.XtraEditors.LabelControl password1Label;
        private DevExpress.XtraEditors.LabelControl password2Label;
        private DevExpress.XtraEditors.LabelControl username;
        private DevExpress.XtraEditors.TextEdit password1;
        private DevExpress.XtraEditors.TextEdit password2;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
    }
}