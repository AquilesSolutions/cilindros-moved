﻿using System;
using System.Windows.Forms;

namespace Cilindros_MRP.Controls
{
    public partial class Calculate : UserControl
    {
        private Src.UserControl user;
        public Calculate(Src.UserControl u)
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            user = u;
        }

        public Delegate sendResults;

        private void calculateButton_Click(object sender, EventArgs e)
        {
            Src.SimulationControl sim = new Src.SimulationControl(user);

            DataSets.Result result = sim.start();
            sendResults.DynamicInvoke(result);
        }
    }
}
