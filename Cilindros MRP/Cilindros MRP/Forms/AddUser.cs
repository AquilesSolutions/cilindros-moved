﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cilindros_MRP.Forms
{
    public partial class AddUser : Form
    {
        Src.UserControl users;
        public AddUser(Src.UserControl u)
        {
            users = u;
            InitializeComponent();
        }

        private void superUserCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (superUserCheck.Checked)
            {
                installations.Enabled = false;
            } else
            {
                installations.Enabled = true;
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            bool super = superUserCheck.Checked;
            bool admin = adminCheck.Checked;

            String details = "";

            if (admin)
            {
                details = "Administrator";
            }
            if (super)
            {
                if (details != "")
                {
                    details += ",SuperUser";
                }
                else
                {
                    details = "SuperUser";
                }
            }
            else
            {
                if (details != "")
                {
                    details += "," + installations.Text;
                }
                else
                {
                    details = installations.Text;
                }
            }
            if (userName.Text == "")
            {
                //status.Text = "A User Name is required";
                MessageBox.Show("A user name is required");
            }
            else if (password.Text == "")
            {
                //status.Text = "A password is required";
                MessageBox.Show("A password is required");
            }
            else if (details == "")
            {
                //status.Text = "Please add installations or set user to Admin or Super User";
                MessageBox.Show("Please add installations or set user to Admin or Super User");
            }

            if (users.addUser(userName.Text, password.Text, details))
            {
                //status.Text = "User " + userName.Text + " added";
                MessageBox.Show("User " + userName.Text + " added");
                Close();
            }
            else
            {
                //status.Text = "User Name " + userName.Text + "is already used";
                MessageBox.Show("User name " + userName.Text + "is already used");
                superUserCheck.Checked = false;
                adminCheck.Checked = false;
                installations.Text = "";
                userName.Text = "";
                password.Text = "";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
