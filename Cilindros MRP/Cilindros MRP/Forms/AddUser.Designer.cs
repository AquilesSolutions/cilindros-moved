﻿namespace Cilindros_MRP.Forms
{
    partial class AddUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.addButton = new DevExpress.XtraEditors.SimpleButton();
            this.superUserCheck = new DevExpress.XtraEditors.CheckEdit();
            this.installations = new DevExpress.XtraEditors.TextEdit();
            this.adminCheck = new DevExpress.XtraEditors.CheckEdit();
            this.password = new DevExpress.XtraEditors.TextEdit();
            this.userName = new DevExpress.XtraEditors.TextEdit();
            this.installationLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.detailsLabel = new DevExpress.XtraEditors.LabelControl();
            this.passwordLabel = new DevExpress.XtraEditors.LabelControl();
            this.UserNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.superUserCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.installations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(22, 16);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(115, 19);
            this.title.TabIndex = 26;
            this.title.Text = "Add New User";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(318, 268);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 24;
            this.addButton.Text = "Add User";
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // superUserCheck
            // 
            this.superUserCheck.Location = new System.Drawing.Point(131, 225);
            this.superUserCheck.Name = "superUserCheck";
            this.superUserCheck.Properties.Caption = "Super User";
            this.superUserCheck.Size = new System.Drawing.Size(75, 19);
            this.superUserCheck.TabIndex = 23;
            this.superUserCheck.CheckedChanged += new System.EventHandler(this.superUserCheck_CheckedChanged);
            // 
            // installations
            // 
            this.installations.Location = new System.Drawing.Point(131, 199);
            this.installations.Name = "installations";
            this.installations.Size = new System.Drawing.Size(262, 20);
            this.installations.TabIndex = 22;
            // 
            // adminCheck
            // 
            this.adminCheck.Location = new System.Drawing.Point(131, 129);
            this.adminCheck.Name = "adminCheck";
            this.adminCheck.Properties.Caption = "User Administrator";
            this.adminCheck.Size = new System.Drawing.Size(112, 19);
            this.adminCheck.TabIndex = 21;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(131, 86);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(262, 20);
            this.password.TabIndex = 20;
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(130, 60);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(263, 20);
            this.userName.TabIndex = 19;
            // 
            // installationLabel
            // 
            this.installationLabel.Location = new System.Drawing.Point(131, 166);
            this.installationLabel.Name = "installationLabel";
            this.installationLabel.Size = new System.Drawing.Size(65, 13);
            this.installationLabel.TabIndex = 18;
            this.installationLabel.Text = "Installations: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(73, 234);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(0, 13);
            this.labelControl5.TabIndex = 17;
            // 
            // detailsLabel
            // 
            this.detailsLabel.Location = new System.Drawing.Point(22, 132);
            this.detailsLabel.Name = "detailsLabel";
            this.detailsLabel.Size = new System.Drawing.Size(39, 13);
            this.detailsLabel.TabIndex = 16;
            this.detailsLabel.Text = "Details: ";
            // 
            // passwordLabel
            // 
            this.passwordLabel.Location = new System.Drawing.Point(22, 89);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(53, 13);
            this.passwordLabel.TabIndex = 15;
            this.passwordLabel.Text = "Password: ";
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.Location = new System.Drawing.Point(22, 63);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(59, 13);
            this.UserNameLabel.TabIndex = 14;
            this.UserNameLabel.Text = "User Name: ";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(223, 268);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 27;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // AddUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 330);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.title);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.superUserCheck);
            this.Controls.Add(this.installations);
            this.Controls.Add(this.adminCheck);
            this.Controls.Add(this.password);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.installationLabel);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.detailsLabel);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.UserNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddUser";
            ((System.ComponentModel.ISupportInitialize)(this.superUserCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.installations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.SimpleButton addButton;
        private DevExpress.XtraEditors.CheckEdit superUserCheck;
        private DevExpress.XtraEditors.TextEdit installations;
        private DevExpress.XtraEditors.CheckEdit adminCheck;
        private DevExpress.XtraEditors.TextEdit password;
        private DevExpress.XtraEditors.TextEdit userName;
        private DevExpress.XtraEditors.LabelControl installationLabel;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl detailsLabel;
        private DevExpress.XtraEditors.LabelControl passwordLabel;
        private DevExpress.XtraEditors.LabelControl UserNameLabel;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
    }
}