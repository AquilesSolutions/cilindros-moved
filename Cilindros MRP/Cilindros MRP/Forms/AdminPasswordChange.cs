﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cilindros_MRP.Forms
{
    public partial class AdminPasswordChange : Form
    {
        String userName;
        Src.UserControl userControl;
        public AdminPasswordChange(string user, Src.UserControl control)
        {
            userName = user;
            userControl = control;
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string p1 = password1.Text;
            string p2 = password2.Text;

            //Password validation - to be decided
            if (p1.Equals(p2.ToString())){
                userControl.updatePassword(userName, p1);
                MessageBox.Show("Password Updated for " + userName);
                Close();
            } else
            {
                MessageBox.Show("New password and confirmation do not match");
                password1.Text = "";
                password2.Text = "";
            }
        }
    }
}
