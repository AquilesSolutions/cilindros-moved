﻿using Cilindros_MRP.Src;
using System;
using System.Windows.Forms;

namespace Cilindros_MRP.Forms
{
    public partial class MainWindow : Form
    {
        private Src.UserControl userControl;
        private String user;
        private bool Administrator, SuperUser = false;
        private String installations = "";

        public MainWindow()
        {
            userControl = new Src.UserControl(this);
            InitializeComponent();
            Shown += logonPrompt;
        }

        public void loadSaved()
        {
            savedSimulations.Items.Clear();

            String[] saved = userControl.getSavedSims();

            for (int i = 0; i < saved.Length; i++)
            {
                savedSimulations.Items.Add(new DevExpress.XtraEditors.Controls.ImageListBoxItem(userControl.getSimName(saved[i])));
            }
        }

        public void enable()
        {
            this.Enabled = true;
        }

        public void disable()
        {
            this.Enabled = false;
        }

        private void logonPrompt(object sender, EventArgs e)
        {
            MainWindow w = (MainWindow)sender;
            w.disable();
            LogonWindow logon1 = new LogonWindow(this);
            logon1.ShowDialog();
        }

        public string checkUser(string userName, string password)
        {
            string result = userControl.logon(userName, password);

            if (result.Equals("true"))
            {
                result = userControl.multiple(userName);
                if (result.Equals("true"))
                {
                    setUserDetails(userName);
                }
            }
            return result;
        }

        public void overRideUser(string userName)
        {
            userControl.overRideUser(userName);
            setUserDetails(userName);
        }

        private void setUserDetails(string userName)
        {
            user = userName;
            String details = userControl.getDetails(userName);
            String[] d = details.Split(',');

            for (int i = 0; i < d.Length; i++)
            {
                if (d[i].Trim().Equals("Administrator"))
                {
                    Administrator = true;
                }
                else if (d[i].Equals("SuperUser"))
                {
                    SuperUser = true;
                }
                installations += d[i];
            }

            if (Administrator)
            {
                navBarGroup1.Visible = true;
            }

            userControl.monitorLogon(userName);
            loadSaved();
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            userControl.logOut(user);
        }

        private void navBar_Click(object sender, EventArgs e)
        {
        }

        private void navBarItem11_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //SimulationControl sim = new SimulationControl();
            contentPanel.Controls.Clear();
            Controls.Calculate solver = new Controls.Calculate(userControl);
            resultsPassed += new passResults(switchToResults);
            solver.sendResults = resultsPassed;
            contentPanel.Controls.Add(solver);
        }

        private void userOverview(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            contentPanel.Controls.Clear();
            Controls.ControlUserOverView viewUserControl = new Controls.ControlUserOverView(userControl);
            contentPanel.Controls.Add(viewUserControl);
        }

        public delegate void passResults(DataSets.Result r);

        private event passResults resultsPassed;

        private void savedSimulations_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraEditors.ImageListBoxControl box = (DevExpress.XtraEditors.ImageListBoxControl)sender;
            loadSim(box);
        }

        private void loadSim(DevExpress.XtraEditors.ImageListBoxControl box)
        {
            if (box.SelectedItem != null)
            {
                string selected = box.SelectedValue.ToString();
                int selectedIndex = box.SelectedIndex;
                SimulationControl loaded;

                if (selected.Contains("AutoSave"))
                {
                    string folder = "Simulation";
                    string number = selected.Substring(10, 1);

                    loaded = userControl.loadSavedSim(folder + number);
                }
                else
                {
                    loaded = userControl.loadSavedSim(selected);
                }

                switchToResults(loaded.getResult);
                box.SelectedIndex = selectedIndex;
            }
        }

        private void savedSimulations_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                DevExpress.XtraEditors.ImageListBoxControl box = (DevExpress.XtraEditors.ImageListBoxControl)sender;
                loadSim(box);
            }
        }

        public void switchToResults(DataSets.Result data)
        {
            contentPanel.Controls.Clear();
            Cilindros_MRP.Controls.ResultsOverview result = new Cilindros_MRP.Controls.ResultsOverview(data);
            contentPanel.Controls.Add(result);
            loadSaved();
        }
    }
}