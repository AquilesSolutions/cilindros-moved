﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Cilindros_MRP.Controls
{
    public partial class ControlUserOverView : UserControl
    {
        Src.UserControl users;
        public ControlUserOverView(Src.UserControl u)
        {
            users = u;
            InitializeComponent();
            Dock = DockStyle.Fill;

            refreshUsers();

        }

        private void refreshUsers()
        {
            userList.Items.Clear();
            DevExpress.XtraEditors.Controls.ImageListBoxItem[] items = new DevExpress.XtraEditors.Controls.ImageListBoxItem[users.Details.Keys.Count];
            for (int i = 0; i < users.Details.Keys.Count; i++)
            {
                items[i] = new DevExpress.XtraEditors.Controls.ImageListBoxItem(users.Details.Keys.ElementAt(i));
            }

            userList.Items.AddRange(items);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Forms.AddUser add = new Forms.AddUser(users);
            Enabled = false;
            add.FormClosed += PopupFormClosed;
            add.Show();
        }

        private void PopupFormClosed(object sender, FormClosedEventArgs e)
        {
            Enabled = true;
            refreshUsers();
        }

        private void userList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (userList.SelectedValue != null)
            {
                string user = userList.SelectedValue.ToString();

                string details = users.getDetails(user);

                if (details.Contains("Administrator"))
                {
                    adminResult.Text = "Yes";
                    int start = details.IndexOf("Administrator");
                    int end = 12;
                    if (start != 0)
                    {
                        start--;
                        if (details.Length > end)
                        {
                            end++;
                        }
                    }
                    else if (details.Length > end)
                    {
                        end++;
                    }

                    details = details.Remove(start, end);


                }
                else
                {
                    adminResult.Text = "No";
                }

                if (details.Contains("SuperUser"))
                {
                    superResult.Text = "Yes";
                    int start = details.IndexOf("SuperUser");
                    int end = 9;
                    if (start != 0)
                    {
                        start--;
                        if (details.Length > end)
                        {
                            end++;
                        }
                    }
                    else if (details.Length > end)
                    {
                        end++;
                    }

                    details = details.Remove(start, end);
                }
                else
                {
                    superResult.Text = "No";
                }
                string[] instals;
                if (details.Length != 0)
                {
                    instals = details.Split(',');
                }
                else
                {
                    instals = new string[] { "None" };
                }
                installtaionsList.Items.Clear();
                DevExpress.XtraEditors.Controls.ImageListBoxItem[] items = new DevExpress.XtraEditors.Controls.ImageListBoxItem[instals.Length];
                for (int i = 0; i < instals.Length; i++)
                {
                    items[i] = new DevExpress.XtraEditors.Controls.ImageListBoxItem(instals[i]);
                }
                installtaionsList.Items.AddRange(items);
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            DialogResult confirm = MessageBox.Show("This will remove the user " + userList.SelectedValue.ToString() + "\r\nAre you sure you want to do this?", "Confirm Removal", MessageBoxButtons.YesNo);
            if(confirm == DialogResult.Yes)
            {
                if (userList.SelectedValue != null)
                {
                    users.removeUser(userList.SelectedValue.ToString());
                    refreshUsers();
                }
            }
        }

        private void detailsButton_Click(object sender, EventArgs e)
        {
            if (userList.SelectedValue != null)
            {
                Forms.UpdateUser update = new Forms.UpdateUser(users, userList.SelectedValue.ToString());
                Enabled = false;
                update.FormClosed += PopupFormClosed;
                update.Show();
            }
        }

        private void passwordButton_Click(object sender, EventArgs e)
        {
            if (userList.SelectedValue != null)
            {
                Forms.AdminPasswordChange pass = new Forms.AdminPasswordChange(userList.SelectedValue.ToString(), users);
                Enabled = false;
                pass.FormClosed += PopupFormClosed;
                pass.Show();
            }
        }
    }
}
