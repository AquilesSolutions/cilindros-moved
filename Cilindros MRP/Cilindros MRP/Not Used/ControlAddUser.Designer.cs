﻿namespace Cilindros_MRP.Controls
{
    partial class ControlAddUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.passwordLabel = new DevExpress.XtraEditors.LabelControl();
            this.detailsLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.installationLabel = new DevExpress.XtraEditors.LabelControl();
            this.userName = new DevExpress.XtraEditors.TextEdit();
            this.password = new DevExpress.XtraEditors.TextEdit();
            this.adminCheck = new DevExpress.XtraEditors.CheckEdit();
            this.installations = new DevExpress.XtraEditors.TextEdit();
            this.superUserCheck = new DevExpress.XtraEditors.CheckEdit();
            this.addButton = new DevExpress.XtraEditors.SimpleButton();
            this.status = new DevExpress.XtraEditors.LabelControl();
            this.title = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.userName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.installations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.superUserCheck.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.Location = new System.Drawing.Point(38, 61);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(59, 13);
            this.UserNameLabel.TabIndex = 0;
            this.UserNameLabel.Text = "User Name: ";
            // 
            // passwordLabel
            // 
            this.passwordLabel.Location = new System.Drawing.Point(38, 115);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(53, 13);
            this.passwordLabel.TabIndex = 1;
            this.passwordLabel.Text = "Password: ";
            // 
            // detailsLabel
            // 
            this.detailsLabel.Location = new System.Drawing.Point(38, 170);
            this.detailsLabel.Name = "detailsLabel";
            this.detailsLabel.Size = new System.Drawing.Size(39, 13);
            this.detailsLabel.TabIndex = 2;
            this.detailsLabel.Text = "Details: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(89, 232);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(0, 13);
            this.labelControl5.TabIndex = 4;
            // 
            // installationLabel
            // 
            this.installationLabel.Location = new System.Drawing.Point(112, 200);
            this.installationLabel.Name = "installationLabel";
            this.installationLabel.Size = new System.Drawing.Size(65, 13);
            this.installationLabel.TabIndex = 5;
            this.installationLabel.Text = "Installations: ";
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(146, 58);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(263, 20);
            this.userName.TabIndex = 6;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(147, 112);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(262, 20);
            this.password.TabIndex = 7;
            // 
            // adminCheck
            // 
            this.adminCheck.Location = new System.Drawing.Point(112, 293);
            this.adminCheck.Name = "adminCheck";
            this.adminCheck.Properties.Caption = "User Administrator";
            this.adminCheck.Size = new System.Drawing.Size(112, 19);
            this.adminCheck.TabIndex = 8;
            // 
            // installations
            // 
            this.installations.Location = new System.Drawing.Point(211, 197);
            this.installations.Name = "installations";
            this.installations.Size = new System.Drawing.Size(198, 20);
            this.installations.TabIndex = 9;
            // 
            // superUserCheck
            // 
            this.superUserCheck.Location = new System.Drawing.Point(211, 242);
            this.superUserCheck.Name = "superUserCheck";
            this.superUserCheck.Properties.Caption = "Super User";
            this.superUserCheck.Size = new System.Drawing.Size(75, 19);
            this.superUserCheck.TabIndex = 10;
            this.superUserCheck.CheckedChanged += new System.EventHandler(this.superUserCheck_CheckedChanged);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(334, 323);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 11;
            this.addButton.Text = "Add User";
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // status
            // 
            this.status.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.status.Location = new System.Drawing.Point(116, 369);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(293, 80);
            this.status.TabIndex = 12;
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(38, 14);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(115, 19);
            this.title.TabIndex = 13;
            this.title.Text = "Add New User";
            // 
            // ControlAddUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.title);
            this.Controls.Add(this.status);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.superUserCheck);
            this.Controls.Add(this.installations);
            this.Controls.Add(this.adminCheck);
            this.Controls.Add(this.password);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.installationLabel);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.detailsLabel);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.UserNameLabel);
            this.Name = "ControlAddUser";
            this.Size = new System.Drawing.Size(451, 468);
            ((System.ComponentModel.ISupportInitialize)(this.userName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.installations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.superUserCheck.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl UserNameLabel;
        private DevExpress.XtraEditors.LabelControl passwordLabel;
        private DevExpress.XtraEditors.LabelControl detailsLabel;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl installationLabel;
        private DevExpress.XtraEditors.TextEdit userName;
        private DevExpress.XtraEditors.TextEdit password;
        private DevExpress.XtraEditors.CheckEdit adminCheck;
        private DevExpress.XtraEditors.TextEdit installations;
        private DevExpress.XtraEditors.CheckEdit superUserCheck;
        private DevExpress.XtraEditors.SimpleButton addButton;
        private DevExpress.XtraEditors.LabelControl status;
        private DevExpress.XtraEditors.LabelControl title;
    }
}
