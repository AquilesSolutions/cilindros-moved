﻿namespace Cilindros_MRP.Forms
{
    partial class UpdateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.updateButton = new DevExpress.XtraEditors.SimpleButton();
            this.superUserCheck = new DevExpress.XtraEditors.CheckEdit();
            this.installations = new DevExpress.XtraEditors.TextEdit();
            this.adminCheck = new DevExpress.XtraEditors.CheckEdit();
            this.installationLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.detailsLabel = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.superUserCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.installations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminCheck.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(233, 280);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 40;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(32, 28);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(101, 19);
            this.title.TabIndex = 39;
            this.title.Text = "Update User";
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(328, 280);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 38;
            this.updateButton.Text = "Update";
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // superUserCheck
            // 
            this.superUserCheck.Location = new System.Drawing.Point(127, 137);
            this.superUserCheck.Name = "superUserCheck";
            this.superUserCheck.Properties.Caption = "Super User";
            this.superUserCheck.Size = new System.Drawing.Size(75, 19);
            this.superUserCheck.TabIndex = 37;
            // 
            // installations
            // 
            this.installations.Location = new System.Drawing.Point(127, 173);
            this.installations.Name = "installations";
            this.installations.Size = new System.Drawing.Size(262, 20);
            this.installations.TabIndex = 36;
            // 
            // adminCheck
            // 
            this.adminCheck.Location = new System.Drawing.Point(115, 66);
            this.adminCheck.Name = "adminCheck";
            this.adminCheck.Properties.Caption = "User Administrator";
            this.adminCheck.Size = new System.Drawing.Size(112, 19);
            this.adminCheck.TabIndex = 35;
            // 
            // installationLabel
            // 
            this.installationLabel.Location = new System.Drawing.Point(115, 109);
            this.installationLabel.Name = "installationLabel";
            this.installationLabel.Size = new System.Drawing.Size(65, 13);
            this.installationLabel.TabIndex = 32;
            this.installationLabel.Text = "Installations: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(83, 246);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(0, 13);
            this.labelControl5.TabIndex = 31;
            // 
            // detailsLabel
            // 
            this.detailsLabel.Location = new System.Drawing.Point(44, 69);
            this.detailsLabel.Name = "detailsLabel";
            this.detailsLabel.Size = new System.Drawing.Size(39, 13);
            this.detailsLabel.TabIndex = 30;
            this.detailsLabel.Text = "Details: ";
            // 
            // UpdateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 330);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.title);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.superUserCheck);
            this.Controls.Add(this.installations);
            this.Controls.Add(this.adminCheck);
            this.Controls.Add(this.installationLabel);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.detailsLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UpdateUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UpdateUser";
            ((System.ComponentModel.ISupportInitialize)(this.superUserCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.installations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminCheck.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.SimpleButton updateButton;
        private DevExpress.XtraEditors.CheckEdit superUserCheck;
        private DevExpress.XtraEditors.TextEdit installations;
        private DevExpress.XtraEditors.CheckEdit adminCheck;
        private DevExpress.XtraEditors.LabelControl installationLabel;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl detailsLabel;
    }
}