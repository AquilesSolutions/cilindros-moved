﻿using System;
using System.Windows.Forms;

namespace Cilindros_MRP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Forms.MainWindow app = new Forms.MainWindow();
            Application.Run(app);
        }


    }
}
