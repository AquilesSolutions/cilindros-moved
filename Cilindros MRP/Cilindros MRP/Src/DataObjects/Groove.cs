﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cilindros_MRP.Src.DataObjects
{
    class Groove
    {
        private string id;
        private int capacity;
        private int max;
        private double security;
        private bool status; //true = can be used, false = needs to be machined

        public Groove(string id, int capacity, double security)
        {
            this.id = id;
            this.capacity = capacity;
            max = capacity;
            this.security = security;
            status = true;
        }

        public String ID
        {
            get { return id; }
        }

        public int Capacity
        {
            get { return capacity; }
            set { capacity = value; }
        }

        public int MaxCapacity
        {
            get { return max; }
        }

        public Double Security
        {
            get { return security; }
        }

        public bool Status
        {
            get { return status; }
            set { status = value; }
        }
    }
}
