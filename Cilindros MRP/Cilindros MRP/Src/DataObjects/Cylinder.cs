﻿using System;

namespace Cilindros_MRP.Src.DataObjects
{
    class Cylinder
    {
        Groove[] grooves;
        Schema schema;
        int avaiableMM;
        int status; //N=0,TO=1,TR=2,RO=3,RE=4,5=Partner Broken
        bool superior;
        String pairID;
        String id;
        private static Random rand = new Random();

        static string idOfPair;

        public Cylinder(string id, Schema s, int size, string stat, String pair, string position)
        {
            this.id = id;
            schema = s;
            avaiableMM = size;
            grooves = schema.Grooves.ToArray();
            pairID = pair;
            if (position.Equals("S")){
                superior = true;
            } else
            {
                superior = false;
            }

            switch (stat)
            {
                case "N":
                    status = 0;
                    break;
                case "TO":
                    status = 1;
                    break;
                case "TR":
                    status = 2;
                    break;
                case "RO":
                    status = 3;
                    break;
                case "RE":
                    status = 4;
                    break;
                default:
                    status = -1;
                    break;
            }


        }

        public Cylinder(Cylinder c)
        {
            grooves = c.Grooves;
            schema = c.Schema;
            avaiableMM = c.avaiableMM;
            status = c.status;
            superior = c.Superior;
            pairID = c.Pair;
            id = c.ID;
        }

        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        public int AvailableToMachine
        {
            get { return avaiableMM; }
        }

        public Schema Schema
        {
            get { return schema; }
        }

        public Groove[] Grooves
        {
            get { return grooves; }
        }

        public String ID
        {
            get { return id; }
        }

        public String Pair
        {
            get { return pairID; }
        }

        public bool Superior
        {
            get { return superior; }
        }

        public bool grovesLeft()
        {
            //Groove[] grooveArray = grooves.Keys.To
            for(int i = 0; i < grooves.Length; i++)
            {
                if (grooves[i].Status)
                {
                    return grooves[i].Status;
                }
            }
            return false;
        }

        public bool machine()
        {
            if (avaiableMM > schema.Consume)
            {
                for (int i = 0; i < grooves.Length; i++)
                {
                    grooves[i].Capacity = grooves[i].MaxCapacity;
                    grooves[i].Status = true;
                }

                avaiableMM = avaiableMM - schema.Consume;
                return true;
            } else
            {
                return false;
            }
        }

        public static string IdToFind
        {
            get { return idOfPair; }
            set { idOfPair = value; }
        }
        public static int CompareBySize(Cylinder x, Cylinder y)
        {
            return x.AvailableToMachine - y.AvailableToMachine;
        }

        public static bool findByID(Cylinder c)
        {
            if (c.Pair.Equals(IdToFind))
            {
                return true;
            }
            return false;
        }

        public bool breaks()
        {
            long timeticks = DateTime.UtcNow.Ticks;
            

            double result = rand.NextDouble();

            //Don't have any breakage data so hard coding 0.5 for now
            if(result < 0.05)
            {
                return true;
            }
            return false;
        }



    }
}
