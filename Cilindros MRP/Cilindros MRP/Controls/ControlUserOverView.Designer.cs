﻿namespace Cilindros_MRP.Controls
{
    partial class ControlUserOverView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userList = new DevExpress.XtraEditors.ImageListBoxControl();
            this.removeButton = new DevExpress.XtraEditors.SimpleButton();
            this.detailsButton = new DevExpress.XtraEditors.SimpleButton();
            this.addButton = new DevExpress.XtraEditors.SimpleButton();
            this.title = new DevExpress.XtraEditors.LabelControl();
            this.userLabel = new DevExpress.XtraEditors.LabelControl();
            this.detailsLabel = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.superResult = new DevExpress.XtraEditors.LabelControl();
            this.adminResult = new DevExpress.XtraEditors.LabelControl();
            this.installtaionsList = new DevExpress.XtraEditors.ImageListBoxControl();
            this.installationsLabel = new DevExpress.XtraEditors.LabelControl();
            this.superLabel = new DevExpress.XtraEditors.LabelControl();
            this.adminLabel = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.passwordButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.userList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.installtaionsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // userList
            // 
            this.userList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.userList.Location = new System.Drawing.Point(33, 86);
            this.userList.MultiColumn = true;
            this.userList.Name = "userList";
            this.userList.Size = new System.Drawing.Size(157, 485);
            this.userList.TabIndex = 0;
            this.userList.SelectedIndexChanged += new System.EventHandler(this.userList_SelectedIndexChanged);
            // 
            // removeButton
            // 
            this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removeButton.Location = new System.Drawing.Point(216, 36);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(100, 23);
            this.removeButton.TabIndex = 1;
            this.removeButton.Text = "Remove User";
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // detailsButton
            // 
            this.detailsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.detailsButton.Location = new System.Drawing.Point(35, 36);
            this.detailsButton.Name = "detailsButton";
            this.detailsButton.Size = new System.Drawing.Size(100, 23);
            this.detailsButton.TabIndex = 2;
            this.detailsButton.Text = "Change Details";
            this.detailsButton.Click += new System.EventHandler(this.detailsButton_Click);
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addButton.Location = new System.Drawing.Point(216, 7);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(100, 23);
            this.addButton.TabIndex = 3;
            this.addButton.Text = "Add New User";
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // title
            // 
            this.title.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(33, 18);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(191, 23);
            this.title.TabIndex = 4;
            this.title.Text = "User Administration";
            // 
            // userLabel
            // 
            this.userLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.userLabel.Location = new System.Drawing.Point(33, 66);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(31, 13);
            this.userLabel.TabIndex = 5;
            this.userLabel.Text = "Users:";
            // 
            // detailsLabel
            // 
            this.detailsLabel.Location = new System.Drawing.Point(214, 66);
            this.detailsLabel.Name = "detailsLabel";
            this.detailsLabel.Size = new System.Drawing.Size(36, 13);
            this.detailsLabel.TabIndex = 6;
            this.detailsLabel.Text = "Details:";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.superResult);
            this.panelControl1.Controls.Add(this.adminResult);
            this.panelControl1.Controls.Add(this.installtaionsList);
            this.panelControl1.Controls.Add(this.installationsLabel);
            this.panelControl1.Controls.Add(this.superLabel);
            this.panelControl1.Controls.Add(this.adminLabel);
            this.panelControl1.Location = new System.Drawing.Point(212, 86);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(391, 443);
            this.panelControl1.TabIndex = 7;
            // 
            // superResult
            // 
            this.superResult.Location = new System.Drawing.Point(90, 67);
            this.superResult.Name = "superResult";
            this.superResult.Size = new System.Drawing.Size(0, 13);
            this.superResult.TabIndex = 5;
            // 
            // adminResult
            // 
            this.adminResult.Location = new System.Drawing.Point(101, 25);
            this.adminResult.Name = "adminResult";
            this.adminResult.Size = new System.Drawing.Size(0, 13);
            this.adminResult.TabIndex = 4;
            // 
            // installtaionsList
            // 
            this.installtaionsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.installtaionsList.Appearance.BackColor = System.Drawing.Color.White;
            this.installtaionsList.Appearance.Options.UseBackColor = true;
            this.installtaionsList.Location = new System.Drawing.Point(23, 128);
            this.installtaionsList.Name = "installtaionsList";
            this.installtaionsList.Size = new System.Drawing.Size(353, 301);
            this.installtaionsList.TabIndex = 3;
            // 
            // installationsLabel
            // 
            this.installationsLabel.Location = new System.Drawing.Point(23, 109);
            this.installationsLabel.Name = "installationsLabel";
            this.installationsLabel.Size = new System.Drawing.Size(65, 13);
            this.installationsLabel.TabIndex = 2;
            this.installationsLabel.Text = "Installations: ";
            // 
            // superLabel
            // 
            this.superLabel.Location = new System.Drawing.Point(23, 67);
            this.superLabel.Name = "superLabel";
            this.superLabel.Size = new System.Drawing.Size(60, 13);
            this.superLabel.TabIndex = 1;
            this.superLabel.Text = "Super User: ";
            // 
            // adminLabel
            // 
            this.adminLabel.Location = new System.Drawing.Point(23, 25);
            this.adminLabel.Name = "adminLabel";
            this.adminLabel.Size = new System.Drawing.Size(71, 13);
            this.adminLabel.TabIndex = 0;
            this.adminLabel.Text = "Administrator: ";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.passwordButton);
            this.panelControl2.Controls.Add(this.addButton);
            this.panelControl2.Controls.Add(this.removeButton);
            this.panelControl2.Controls.Add(this.detailsButton);
            this.panelControl2.Location = new System.Drawing.Point(251, 535);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(352, 74);
            this.panelControl2.TabIndex = 8;
            // 
            // passwordButton
            // 
            this.passwordButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordButton.Location = new System.Drawing.Point(35, 7);
            this.passwordButton.Name = "passwordButton";
            this.passwordButton.Size = new System.Drawing.Size(99, 23);
            this.passwordButton.TabIndex = 4;
            this.passwordButton.Text = "Change Password";
            this.passwordButton.Click += new System.EventHandler(this.passwordButton_Click);
            // 
            // ControlUserOverView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.detailsLabel);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.title);
            this.Controls.Add(this.userList);
            this.Name = "ControlUserOverView";
            this.Size = new System.Drawing.Size(645, 624);
            ((System.ComponentModel.ISupportInitialize)(this.userList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.installtaionsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ImageListBoxControl userList;
        private DevExpress.XtraEditors.SimpleButton removeButton;
        private DevExpress.XtraEditors.SimpleButton detailsButton;
        private DevExpress.XtraEditors.SimpleButton addButton;
        private DevExpress.XtraEditors.LabelControl title;
        private DevExpress.XtraEditors.LabelControl userLabel;
        private DevExpress.XtraEditors.LabelControl detailsLabel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ImageListBoxControl installtaionsList;
        private DevExpress.XtraEditors.LabelControl installationsLabel;
        private DevExpress.XtraEditors.LabelControl superLabel;
        private DevExpress.XtraEditors.LabelControl adminLabel;
        private DevExpress.XtraEditors.LabelControl superResult;
        private DevExpress.XtraEditors.LabelControl adminResult;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton passwordButton;
    }
}
