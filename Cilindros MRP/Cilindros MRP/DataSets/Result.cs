﻿namespace Cilindros_MRP.DataSets
{


    partial class Result
    {
        public static int CompareByPurchases(Result x, Result y)
        {

            int totalx = 0;
            int totaly = 0;

            for (int i = 0; i < x.Results.Rows.Count; i++)
            {
                object[] row = x.Results.Rows[i].ItemArray;
                totalx += int.Parse(row[5].ToString());
            }

            for (int i = 0; i < y.Results.Rows.Count; i++)
            {
                object[] row = y.Results.Rows[i].ItemArray;
                totaly += int.Parse(row[5].ToString());
            }

            return totalx - totaly;
        }
    }
}
