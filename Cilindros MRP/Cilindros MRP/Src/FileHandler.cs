﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Cilindros_MRP.Src
{
    internal class FileHandler
    {
        private ReaderWriterLockSlim fileLock = new ReaderWriterLockSlim();
        private int timeOut;
        private static int simulationCount = 0;

        public FileHandler(int timeout)
        {
            timeOut = timeout;
        }

        public void writeToFile(string path, byte[] userbytes)
        {
            if (fileLock.TryEnterWriteLock(timeOut))
            {
                using (FileStream f = createFile(path))
                {
                    try
                    {
                        for (int i = 0; i < userbytes.Length; i++)
                        {
                            f.WriteByte(userbytes[i]);
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        fileLock.ExitWriteLock();
                    }
                }
            }
        }

        public bool existsFile(string path)
        {
            return File.Exists(path);
        }

        public bool existsDirectory(string path)
        {
            return Directory.Exists(path);
        }

        public void directroyDelete(string path)
        {
            Directory.Delete(path, true);
        }

        public FileStream createFile(string path)
        {
            if (existsFile(path))
            {
                delete(path);
            }
            FileStream f = File.Create(path);
            File.SetAttributes(path, FileAttributes.Hidden);
            return f;
        }

        public void createDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                DirectoryInfo d = Directory.CreateDirectory(path);
                d.Attributes = FileAttributes.Directory | FileAttributes.Hidden | FileAttributes.NotContentIndexed;
            }
        }

        public string readFromFile(string path)
        {
            if (existsFile(path))
            {
                byte[] bytes = null;
                if (fileLock.TryEnterReadLock(timeOut))
                {
                    try
                    {
                        bytes = File.ReadAllBytes(path);
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                    finally
                    {
                        fileLock.ExitReadLock();
                    }
                }

                return Encoding.UTF8.GetString(bytes);
            }
            return "false";
        }

        public void delete(string path)
        {
            File.Delete(path);
        }

        public bool concurrentAllowAccessCheck(string path, string f, int timeout)
        {
            List<string> start = new List<string>();
            DateTime now = DateTime.UtcNow;

            string fileName = f + "," + now.Ticks + "," + Environment.MachineName;
            FileStream file = createFile(path + fileName);
            file.Close();
            int count = 0;

            do
            {
                Random waitGenerator = new Random();
                int wait = waitGenerator.Next(100);
                count += wait;
                Thread.Sleep(wait);
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (FileInfo fileinfo in dir.GetFiles(f + ",*.*"))
                {
                    start.Add(fileinfo.Name);
                }
                start.Sort();
            } while (!start[0].Equals(fileName.ToString()) && count < timeout);

            //Timeout
            if (count > timeout)
                return false;

            return true;
        }

        public bool releaseAccess(string path, string file)
        {
            string fileName = "";

            DirectoryInfo dir = new DirectoryInfo(path);
            foreach (FileInfo f in dir.GetFiles("*.*"))
            {
                string[] components = f.Name.Split(',');
                if (components[0].Equals(file.ToString()) && components[components.Length - 1].Equals(Environment.MachineName))
                {
                    fileName = f.Name;
                }
            }

            if (fileName != "")
            {
                delete(path + fileName);
                return true;
            }
            return false;
        }

        public void WriteData(System.Data.DataSet data, String path)
        {
            data.WriteXml(createFile(path));
        }

        public void ReadData(System.Data.DataSet data, String path)
        {
            data.ReadXml(path);
        }

        public String[] getDataFiles(String path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            String[] results = new String[dir.GetFiles("*.dat").Length];

            for (int i = 0; i < results.Length; i++)
            {
                results[i] = dir.GetFiles("*.dat")[i].Name;
            }

            return results;
        }

        public void autoSaveSimulation(String path, DataSets.DataSource source, List<DataSets.Result> results, List<Simulation> sims)
        {
            String directory = path + "Simulation" + simulationCount;

            if (concurrentAllowAccessCheck(path, "Simulation" + simulationCount, 1000))
            {
                if (!existsDirectory(directory))
                {
                    createDirectory(directory);
                }
                else
                {
                    directroyDelete(directory);
                    createDirectory(directory);
                }

                createFile(directory + "/timestamp" + DateTime.Now.Ticks);

                WriteData(source, directory + "/source");

                for (int i = 0; i < results.Count; i++)
                {
                    WriteData(results[i], directory + "/result" + i);
                }

                releaseAccess(path, "Simulation" + simulationCount);

                simulationCount++;

                if (simulationCount > 4)
                {
                    simulationCount = 0;
                }

                
            }
        }

        public void SaveSimulation(String path, String fileName, DataSets.DataSource source, List<DataSets.Result> results, List<Simulation> sims)
        {
            String directory = path + fileName;

            if (concurrentAllowAccessCheck(path, fileName, 1000))
            {
                if (!existsDirectory(directory))
                {
                    createDirectory(directory);
                }
                else
                {
                    directroyDelete(directory);
                    createDirectory(directory);
                }

                createFile(directory + "/" +fileName);

                WriteData(source, directory + "/source");

                for (int i = 0; i < results.Count; i++)
                {
                    WriteData(results[i], directory + "/result" + i);
                }

                releaseAccess(path, fileName);
            }
        }

        public SimulationControl loadSavedSimulation(String path, UserControl user)
        {
            DirectoryInfo di = new DirectoryInfo(path);

            List<DataSets.Result> results = new List<DataSets.Result>();
            DataSets.DataSource source = new DataSets.DataSource();


            foreach(FileInfo file in di.GetFiles())
            {
                if (file.Name.Equals("source"))
                {
                    ReadData(source, file.FullName);
                } else if (file.Name.Contains("timestamp")){
                    // do nothing
                } else
                {
                    DataSets.Result r = new DataSets.Result();
                    ReadData(r, file.FullName);
                    results.Add(r);
                }
            }

            return new SimulationControl(results, source, user);
        }

        public String[] getSavedSims(String path)
        {
            return Directory.GetDirectories(path);
        }

        public string getSimulationName(String path)
        {
            string[] foldername = path.Split('/');


            if(existsFile(path + "/" + foldername[foldername.Length - 1]))
            {
                return foldername[foldername.Length - 1];
            } else
            {
                DirectoryInfo di = new DirectoryInfo(path);

                foreach(FileInfo file in di.GetFiles())
                {
                    if (file.Name.Contains("timestamp"))
                    {
                        string time = file.Name.Substring(9);
                        string number = foldername[foldername.Length - 1].Substring(10);

                        DateTime saved = new DateTime(long.Parse(time));

                        return "AutoSave 0" + number + " " + saved.ToString();
                    }
                }
            }

            return "error";
        }
    }
}