﻿using System;
using System.Collections.Generic;
using Cilindros_MRP.Src.DataObjects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Cilindros_MRP.Src
{
    public class Simulation
    {
        private DataSets.DataSource data;
        private DataSets.Result result;

        private List<CylindersByMonth> totalUsed = new List<CylindersByMonth>();
        private List<Cylinder> allCylinders = new List<Cylinder>();
        private List<Purchase> purchases = new List<Purchase>();

        int brokencount = 0;

        private Dictionary<DateTime, List<Product>> productionData;

        int name = 1;

        public Simulation(DataSets.DataSource data)
        {
            this.data = data;
            productionData = new Dictionary<DateTime, List<Product>>();
            setup();
            result = new DataSets.Result();
        }

        private Schema createNewSchema(String ID)
        {

            object[] schemaData = data.Schemas.FindBySchemaID(ID).ItemArray;

            String[] grooveStrings = new String[5];

            int count = 0;
            int grooveIndex = 0;
            int countIndex = 5;
            while (count < 5 && countIndex < 10)
            {
                for (int j = 0; j < int.Parse(schemaData[countIndex].ToString()); j++)
                {
                    grooveStrings[count] = schemaData[grooveIndex].ToString();
                    count++;
                }
                countIndex++;
                grooveIndex++;
            }

            List<Groove> grooves = new List<Groove>();

            for (int j = 0; j < grooveStrings.Length; j++)
            {
                if (grooveStrings[j] != null)
                {
                    Groove g = new Groove(grooveStrings[j], data.Groove.FindById(grooveStrings[j]).Capacity, data.Groove.FindById(grooveStrings[j]).Securtiy);
                    grooves.Add(g);
                }
            }

            // Create a schema
            return new Schema(ID, grooves, data.Schemas.FindBySchemaID(ID).Consume);
        }

        private void setup()
        {
            // Inventory
            for(int i = 0; i < data.Inventory.Rows.Count; i++)
            {
                object[] row = data.Inventory.Rows[i].ItemArray;
                String schemaId = row[1].ToString();

                // Create a schema
                Schema s = createNewSchema(schemaId);

                // Add cylinder
                allCylinders.Add(new Cylinder(row[0].ToString(), s, int.Parse(row[2].ToString()), row[3].ToString(), row[4].ToString(), row[5].ToString()));
            }


            // Production Data
            for (int i = 0; i < 12; i++)
            {
                productionData.Add((DateTime)data.Period.Rows[i].ItemArray[0], new List<Product>());
            }

            for (int i = 0; i < data.Quantities.Rows.Count; i++)
            {
                object[] row = data.Quantities.Rows[i].ItemArray;

                String product = row[0].ToString();

                List<String> grooves = new List<string>();
                List<Double> ratios = new List<double>();

                object[] row2 = data.Ratios.FindByProduct(product).ItemArray;

                for(int j = 1; j < row2.Length; j +=2)
                {
                    grooves.Add(row2[j].ToString());
                    ratios.Add(double.Parse(row2[j + 1].ToString()));
                }

                for(int j = 1; j < row.Length; j++)
                {
                    productionData.Values.ElementAt(j-1).Add(new Product(product, int.Parse(row[j].ToString()), grooves, ratios));
                }
            }


        }

        public DataSets.Result Start()
        {
            for(int i = 0; i < productionData.Count; i++)
            {
                month(productionData.Keys.ElementAt(i), productionData.Values.ElementAt(i));
            }

            DataTable results = result.Tables["Results"];

            /*Results columns

              < DesignColumnRef Name = "Month" />
              < DesignColumnRef Name = "Product" />
              < DesignColumnRef Name = "Qunatity" />
              < DesignColumnRef Name = "CylindersUsed" />
              < DesignColumnRef Name = "CylinersIventoried" />
              < DesignColumnRef Name = "CylindersPurchase" />
              < DesignColumnRef Name = "CylindersBroken" />
              < DesignColumnRef Name = "CylindersMachined" />
             */

            DataRow row; 

            //foreach month
            foreach(DateTime month in productionData.Keys)
            {
                CylindersByMonth.MonthToFind = month;
                Purchase.MonthToFind = month;
                //foreach product
                for (int i = 0; i < productionData[month].Count; i++)
                {
                    row = results.NewRow();
                    row[0] = month;
                    row[1] = productionData[month][i].ProductGroup;
                    row[2] = productionData[month][i].Quantity;

                    CylindersByMonth m = totalUsed.Find(CylindersByMonth.FindByMonth);

                    UsedCylindersByProduct.ProductToFind = productionData[month][i].ProductGroup;
                    row[3] = m.ProductCylinderList.Find(UsedCylindersByProduct.FindByProduct).CylinderList.Count;

                    //Inventory
                    row[4] = m.Inventory.Count;

                    //Purchases
                    row[5] = purchases.FindAll(Purchase.FindByMonth).Count;
                    results.Rows.Add(row);

                    //Broken
                    row[6] = m.Broken.Count;
                }
            }

            return result;
        }

        private void month(DateTime date, List<Product> products)
        {
            // add a new item to totalUsed for each month
            CylindersByMonth thisMonth = new CylindersByMonth(date);

            for(int i =0; i < products.Count; i++)
            {
                product(products[i], thisMonth.ProductCylinderList, date, thisMonth.Broken);
            }

            totalUsed.Add(thisMonth);

            // NOTE: at the end of the month check for cylinders that were used that need machining


            //Replace allCylinders with their used versions
            for(int i = 0; i < thisMonth.ProductCylinderList.Count; i++)
            {
                for(int j = 0; j < thisMonth.ProductCylinderList[i].CylinderList.Count; j++)
                {
                    String id = thisMonth.ProductCylinderList[i].CylinderList[j].ID;

                    for(int k = 0; k < allCylinders.Count; k++)
                    {
                        if (allCylinders[k].ID.Equals(id))
                        {
                            allCylinders[k] = thisMonth.ProductCylinderList[i].CylinderList[j];
                        }
                    }

                }
            }

            // Machine any cylinders that were completely used
            for(int i = 0; i < allCylinders.Count; i++)
            {
                // Machine any cylinders that were completely used
                if (!allCylinders[i].grovesLeft())
                {
                    if (!allCylinders[i].machine())
                    {
                        //means the cylinder is completely used up;
                        //finished.Add(allCylinders[i]);
                        //allCylinders.RemoveAt(i);

                        allCylinders[i].Status = 4;
                    }
                }


                //copy current inventory to per Month list unless its broken or used or its partner is broken
                if (allCylinders[i].Status < 3)
                {
                    Cylinder copy = new Cylinder(allCylinders[i]);
                    thisMonth.Inventory.Add(copy);
                }
            }

            // Resort the list

            allCylinders.Sort(Cylinder.CompareBySize);
        }

        private void product(Product toProduce, List<UsedCylindersByProduct> used, DateTime date, List<Cylinder> broken)
        {
            // NOTE: When calculating cylinders available for a groove the finished ones in the grooves used list and the broken ones should not be sent

            UsedCylindersByProduct productUsed = new UsedCylindersByProduct(toProduce.ProductGroup);

            //Loop through the grooves for the product
            for(int i = 0; i < toProduce.Gooves.Count; i++)
            {
                //For each Groove find a list of cylinders that can be used
                List<Cylinder> correctGrooves = new List<Cylinder>();
                for (int j = 0; j < allCylinders.Count; j++)
                {
                    bool add = false;
                    //find the cylinders with the right grooves
                    for(int k = 0; k < allCylinders[j].Grooves.Length; k++)
                    {
                        //Check the groove is the same and that it is not all used
                        if (allCylinders[j].Grooves[k].ID.Equals(toProduce.Gooves[i])  && allCylinders[j].Grooves[k].Status)
                        {
                            //check that the cylinder is not broken or retired
                            if(allCylinders[j].Status < 3)
                                add = true;
                        }
                    }
                    //Check the cylinder has not already been used for this product this month
                    for (int l = 0; l < productUsed.CylinderList.Count; l++)
                    {
                        if (productUsed.CylinderList[l].ID.Equals(allCylinders[j].ID))
                        {
                            add = false;
                            break;
                        }
                    }
                    // If it has the right grooves and is not in the used list add to avaiable cylinders
                    if (add)
                    {                     
                        correctGrooves.Add(allCylinders[j]);
                    }
                }


                //Calculate Groove
                int amountForGroove = (int)Math.Ceiling(toProduce.Quantity * toProduce.Ratio[i]);

                Groove(toProduce.Gooves[i], amountForGroove, ref correctGrooves, ref productUsed, date, broken);
                
            }
            used.Add(productUsed);



        }

		private void Groove(String groove, int tonnes, ref List<Cylinder> cylinders, ref UsedCylindersByProduct used, DateTime date, List<Cylinder> broken)
		{

			int remainingProduction = tonnes;//(int)Math.Ceiling(tonnes * example.Security);
            bool securityAdded = false;


			while (remainingProduction > 0)
			{
			    if(cylinders.Count == 0)
				{
                    //Need to purchase a Cylinder
                    String schema = null;
                    for(int i = 0; i < data.Schemas.Rows.Count; i++)
                    {
                        object[] row = data.Schemas.Rows[i].ItemArray;
                        for(int j = 0; j< 5; j++)
                        {
                            if (row[j].ToString().Equals(groove))
                            {
                                schema = row[11].ToString();
                                break;
                            }
                        }
                        if(schema != null)
                        {
                            break;
                        }
                    }

                    int consume = data.Schemas.FindBySchemaID(schema).Consume;
                    Cylinder newSuperior = new Cylinder("new" + name, createNewSchema(schema), 50, "N", "new"+(name+1), "S");
                    name++;
                    Cylinder newInferior = new Cylinder("new" + name, createNewSchema(schema), 50, "N", "new" + (name -1), "I");
                    name++;
                    Purchase p1 = new Purchase(newSuperior, date);
                    Purchase p2 = new Purchase(newInferior, date);

                    cylinders.Add(newSuperior);
                    purchases.Add(p1);
                    cylinders.Add(newInferior);
                    purchases.Add(p2);
                    //Add to overall list of cylinders so its avaiable for the next month/product
                    allCylinders.Add(newSuperior);
                    allCylinders.Add(newInferior);
                }

				//Chose a cylinder - currently this assumes that the list has been sorted from least capacity to most
				Cylinder chosen = cylinders[cylinders.Count-1];
                Cylinder.IdToFind = chosen.Pair;
                int pairIndex = cylinders.FindIndex(Cylinder.findByID);
                Cylinder pair = cylinders[pairIndex];

                cylinders.RemoveAt(pairIndex);
                if (cylinders.Count > 0)
                {
                    cylinders.RemoveAt(cylinders.Count - 1);
                }

                //Check for breakage

                if (chosen.breaks())
                {
                    chosen.Status = 3;
                    pair.Status = 5;
                    brokencount++;
                    broken.Add(chosen);
                } else if (pair.breaks())
                {
                    pair.Status = 3;
                    chosen.Status = 5;
                    brokencount++;
                    broken.Add(pair);
                } else {
				    Groove[] g = chosen.Grooves;
				    Groove grooveToUse = null;
                    int index = 0;
				    for (int i = 0; i < g.Length; i++)
                    {
                        //Check if its a groove we want and can be used
                        if (g[i].ID.Equals(groove) && g[i].Status)
                        {
                            grooveToUse = g[i];
                            index = i;
                            break;
                        }
	    			}

                    Groove pairGroove = pair.Grooves[index];

                    if(grooveToUse != null)
                    {
                        if (!securityAdded)
                        {
                            remainingProduction = (int)Math.Ceiling(remainingProduction * grooveToUse.Security);
                            securityAdded = true;
                        }

                        remainingProduction = remainingProduction - grooveToUse.Capacity;

                        if(remainingProduction < 0)
                        {
                            grooveToUse.Capacity = remainingProduction * -1;
                            pairGroove.Capacity = remainingProduction * -1;
                        } else
                        {
                            grooveToUse.Status = false;
                            grooveToUse.Capacity = 0;
                            pairGroove.Status = false;
                            pairGroove.Capacity = 0;
                        }

                        used.CylinderList.Add(chosen);
                        used.CylinderList.Add(pair);
                    }
                }
            }
		}

        class Purchase
        {
            Cylinder cylinder;
            DateTime dateNeeded;
            static DateTime dateToFind;

            public Purchase(Cylinder cylinder, DateTime date)
            {
                this.cylinder = cylinder;
                dateNeeded = date;
            }

            public static DateTime MonthToFind
            {
                get { return dateToFind; }
                set { dateToFind = value; }
            }

            public static bool FindByMonth(Purchase p)
            {
                if (p.dateNeeded.Month.Equals(MonthToFind.Month))
                {
                    return true;
                }

                return false;
            }
        }

        class CylindersByMonth
        {
            List<UsedCylindersByProduct> cylindersByProduct;
            List<Cylinder> inventory;
            List<Cylinder> broken;
            DateTime date;
            static DateTime dateToFind;

            public CylindersByMonth(DateTime date)
            {
                cylindersByProduct = new List<UsedCylindersByProduct>();
                inventory = new List<Cylinder>();
                broken = new List<Cylinder>();
                this.date = date;
            }

            public List<Cylinder> Inventory
            {
                get { return inventory; }
            }

            public List<Cylinder> Broken
            {
                get { return broken; }
            }

            public List<UsedCylindersByProduct> ProductCylinderList
            {
                get { return cylindersByProduct; }
                set { cylindersByProduct = value; }
            }

            public static DateTime MonthToFind
            {
                get { return dateToFind; }
                set { dateToFind = value; }
            }

            public static bool FindByMonth(CylindersByMonth month)
            {
                if (month.date.Month.Equals(MonthToFind.Month))
                {
                    return true;
                }

                return false;
            }

        }

        class UsedCylindersByProduct
        {
            List<Cylinder> cylinders;
            String productId;
            static string productToFind;

            public UsedCylindersByProduct(String product)
            {
                productId = product;
                cylinders = new List<Cylinder>();
            }

            public static string ProductToFind
            {
                get { return productToFind; }
                set { productToFind = value; }
            }

            public List<Cylinder> CylinderList
            {
                get { return cylinders; }
                set { cylinders = value; }
            }

            public static bool FindByProduct(UsedCylindersByProduct prod)
            {
                if (prod.productId.Equals(ProductToFind))
                {
                    return true;
                }

                return false;
            }
        }
    }
}
