﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cilindros_MRP.Forms
{
    public partial class UpdateUser : Form
    {
        Src.UserControl users;
        String username;
        public UpdateUser(Src.UserControl u, string username)
        {
            users = u;
            this.username = username;
            InitializeComponent();

            string[] current = users.getDetails(username).Split(',');

            for(int i = 0; i < current.Length; i++)
            {
                switch (current[i])
                {
                    case "Administrator":
                        adminCheck.Checked = true;
                        break;
                    case "SuperUser":
                        superUserCheck.Checked = true;
                        break;
                    default:
                        if(installations.Text == "")
                        {
                            installations.Text = current[i];
                        } else
                        {
                            installations.Text += "," + current[i];
                        }
                        break;
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            string result = "";
            if (adminCheck.Checked)
            {
                result = "Administrator";
            }

            if (superUserCheck.Checked)
            {
                if(result == "")
                {
                    result = "SuperUser";
                } else
                {
                    result += ",SuperUser";
                }
            }

            if (result != "" && installations.Text != "")
                result += ",";

            result += installations.Text;

            users.updateDetails(username, result);
            Close();
        }
    }
}
