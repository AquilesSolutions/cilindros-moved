﻿using System;
using System.Data;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace Cilindros_MRP.Src
{
    class CustomPivotGrid
    {
        private PanelControl panel;
        private PivotGridControl pivotGridControl1;
        private BindingSource dataBindingSource;

        public CustomPivotGrid(PanelControl panel, DataSet data, String table)
        {
            String[] tableNames = new String[data.Tables.Count];
            String[][] fields = new String[data.Tables.Count][];
            int position = 0;

            for (int i = 0; i < data.Tables.Count; i++)
            {
                tableNames[i] = data.Tables[i].TableName;
                if (tableNames[i] == table)
                {
                    position = i;
                }
                fields[i] = new String[data.Tables[i].Columns.Count];
                for (int j = 0; j < data.Tables[i].Columns.Count; j++)
                {
                    fields[i][j] = data.Tables[i].Columns[j].ColumnName;
                }
            }




            this.panel = panel;

            dataBindingSource = setData(data);
            pivotGridControl1 = createPivotGrid(table, fields[position], panel);
            panel.Controls.Add(pivotGridControl1);

            panel.Dock = DockStyle.Fill;
            pivotGridControl1.Dock = DockStyle.Fill;
        }

        private BindingSource setData(DataSet dataSet)
        {
            BindingSource data = new BindingSource();
            ((System.ComponentModel.ISupportInitialize)(data)).BeginInit();

            data.DataSource = dataSet;
            data.Position = 0;

            ((System.ComponentModel.ISupportInitialize)(data)).EndInit();
            return data;
        }

        private PivotGridControl createPivotGrid(String table, String[] fields, PanelControl panel)
        {
            PivotGridControl grid = new PivotGridControl();
            grid.Dock = DockStyle.Fill;

            PivotGridField[] gridfields = new PivotGridField[fields.Length];

            //Fields
            for (int i = 0; i < fields.Length; i++)
            {
                gridfields[i] = new PivotGridField();
                gridfields[i].AreaIndex = i;
                gridfields[i].Caption = fields[i];
                gridfields[i].FieldName = fields[i];
                switch (gridfields[i].FieldName)
                {
                    case "Month":
                        gridfields[i].Area = PivotArea.RowArea;
                        break;

                    case "Product":
                        gridfields[i].Area = PivotArea.ColumnArea;
                        break;

                    case "Quantity":
                    case "CylindersMachined":
                        break;

                    default:
                        gridfields[i].Area = PivotArea.DataArea;
                        if (gridfields[i].FieldName.Equals("CylindersPurchased")){
                            gridfields[i].AreaIndex = 0;
                        } else if (gridfields[i].FieldName.Equals("CylindersUsed")){
                            gridfields[i].AreaIndex = 1;
                        } else if (gridfields[i].FieldName.Equals("CylindersBroken"))
                        {
                            gridfields[i].AreaIndex = 2;
                        } else if (gridfields[i].FieldName.Equals("CylindersIventoried"))
                        {
                            gridfields[i].AreaIndex = 3;
                        }
                        break;
                }

                gridfields[i].Name = "gridfeilds" + i;
            }

            //Grid
            ((System.ComponentModel.ISupportInitialize)(grid)).BeginInit();
            grid.DataSource = dataBindingSource;
            grid.DataMember = table;
            grid.Fields.AddRange(gridfields);
            grid.Location = new System.Drawing.Point(0, 0);
            grid.Name = "pivotGridControl" + table;
            grid.Size = new System.Drawing.Size(829, 366);
            grid.TabIndex = 0;

           

            ((System.ComponentModel.ISupportInitialize)(grid)).EndInit();

            return grid;
        }

    }
}
