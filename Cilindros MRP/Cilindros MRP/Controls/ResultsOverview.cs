﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cilindros_MRP.Controls
{
    public partial class ResultsOverview : UserControl
    {
        public ResultsOverview(DataSets.Result result)
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            Src.CustomPivotGrid grid = new Src.CustomPivotGrid(panelControl1, result, "Results");
        }
    }
}
